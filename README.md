# LabCOAT DAC ML2 #



## Abstract with the Definition of the Problem to Solve and Resolution ##


**LabCOAT** (**Lab**oratory **C**oncepts **O**rganized **A**s **T**echnology) is an organization dedicated to the promotion & advancement of science
via the automation and virtualization of lab experimentation and research submission.

**LabCOAT DAC ML2** is a *Decentralized Autonomous Corporation* comprised of self-governing scientists, engineers, and stakeholders 
guided by rules encoded in computer programs called smart contracts that reside on the Ethereum-compatible **Metis Layer 2** blockchain. 

**LabCOAT** will streamline, virtualize, and automate the workflow of conducting scientific research:

* the formulation of hypotheses for experiments

* the operation of lab equipment

* collection of data from devices and experiment results

* AI/ML model creation for predictions and analysis

* Jupyter notebook generation for reports

* research submission for publication and peer review

**LabCOAT** will perform regulatory and administrative duties with its members according to **ML2** rules on the chain.

The **LabCOAT** community will promote scientific education and endeavors by lobbying for science-based legislation and raising funds for research & development.

The format of current research “papers” published in science journals is antiquated, static, and needlessly complicated with inpenetrable jargon and syntactic obfuscation.

These “papers” will be replaced with dynamic, open, and update-able digital “documents” integrated with interactive Jupyter notebooks and data science visualizations.

**LabCOAT** will provide templates for research documents and host version controlled repositories of works-in-progress and verified research documents.

Novel research presented in science journals will be duplicated and verified in labs sponsored and certified by **LabCOAT** and other regulatory groups.

**LabCOAT** will issue tokens to support the operation of the ecosystem.

IPFS integration will allow the storage and sharing of files and data.